package com.cafe.cafeDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAutoConfiguration
@SpringBootApplication
public class CafeDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CafeDemoApplication.class, args);
	}
}
